% trying by hand, and if nothing goes well will pick apart
% "audiopluginexample.private.AnalysisAndSynthesisBuffer"

classdef AnalysisSynthesisTemplate < audioPlugin
    properties (Access = public)
        hopSize = 64;
        windowSize = 256;
    end
    
    properties (Access = private)
        % window vector
        window;
        
        % buffer dimensions
        numBufChans;
        bufferSize;
        
        % buffers
        anBuf;
        synBuf;
        
        % buffer rw positions
        anBufWritePos;
        synBufReadPos;
    end
    
    methods
        
        % constructor
        function plugin = AnalysisSynthesisTemplate
            
            % as many buffer channels as hop sizes in the buffer size
            plugin.numBufChans = ceil(plugin.windowSize/plugin.hopSize);
            plugin.bufferSize = plugin.windowSize;
            
            % analysis and synthasis buffers init
            plugin.anBuf = zeros(plugin.bufferSize, plugin.numBufChans);
            plugin.synBuf = plugin.anBuf;
            
            % window init
            plugin.window = hann(plugin.windowSize) / sqrt(plugin.numBufChans);
            
            % rw positions ---- this is the golden ticket
            plugin.anBufWritePos = [1 (cumsum(ones(plugin.numBufChans-1, 1))'*plugin.hopSize)+1];
            plugin.synBufReadPos = plugin.anBufWritePos;
        end
        
        function y = spectral(plugin, x)
            
            % is bufferSize even or odd
            N = 0;
            if mod(length(x), 2) == 0
                N = length(x)/2 + 1;
            else
                N = ceil(length(x)/2);
            end
            
            % enter spectral domain
            X = fft(x);
            Y = X(1:N);
            
            % do something
            % ........
            
            % lets do lpf, like maxh's
            Y(((N-1)/16):end) = 0;

            % simplest
%             Y(:) = 0;
%             Y(round(N/8)) = 400;
            
            % return to time domain
            Y = [Y; conj(flip(Y(2:end-1)))];
            y = real(ifft(Y));
            
        end
        
        % asynchronous buffer
        function out = process(plugin,in)

            % input size
            [numSamples, numChannels] = size(in);
            
            % output block
            out = zeros(numSamples, numChannels);
            
            % iterate through samples
            for n = 1:numSamples
                
                % write to buffers
                for b = 1:plugin.numBufChans
                    
                    % write to b channel
                    plugin.anBuf(plugin.anBufWritePos(b), b) = in(n);
                    
                    % if write pos is at end of buffer
                    % perform asynchronous processing
                    if plugin.anBufWritePos(b) == plugin.bufferSize
                        
                        % copy to synBuf 
                        plugin.synBuf(:, b) = plugin.anBuf(:, b);
                        
                        % apply winfow
                        plugin.synBuf(:, b) = plugin.synBuf(:, b) .* plugin.window;
                        
                        % process
                        plugin.synBuf(:, b) = plugin.spectral(plugin.synBuf(:,b));
                    end
                    
                    out(n, :) = out(n, :) + plugin.synBuf(plugin.synBufReadPos(b), b);
                end
                
                plugin.anBufWritePos = mod(plugin.anBufWritePos, ...
                                        plugin.bufferSize) + 1;
                plugin.synBufReadPos = mod(plugin.synBufReadPos, ...
                                        plugin.bufferSize) + 1;
            end
        end
    end
end
