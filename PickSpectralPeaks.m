
classdef PickSpectralPeaks < audioPlugin
    properties (Access = public)
        hopSize = 128;
        windowSize = 1024;
        numPeaks = 50;
    end
    
    properties (Access = private)
        % window vector
        window;
        
        % buffer dimensions
        numBufChans;
        bufferSize;
        
        % buffers
        anBuf;
        synBuf;
        
        % buffer rw positions
        anBufWritePos;
        synBufReadPos;
    end
    
    properties (Constant)
        PluginInterface = audioPluginInterface( ...           % <== (3) Map tunable property to plugin parameter.
            audioPluginParameter('numPeaks', ...
                'Mapping',{'pow', 1, 1, 513}));
    end
    
    methods
        
        % constructor
        function plugin = PickSpectralPeaks
            
            % as many buffer channels as hop sizes in the buffer size
            plugin.numBufChans = ceil(plugin.windowSize/plugin.hopSize);
            plugin.bufferSize = plugin.windowSize;
            
            % analysis and synthasis buffers init
            plugin.anBuf = zeros(plugin.bufferSize, plugin.numBufChans);
            plugin.synBuf = plugin.anBuf;
            
            % window init
            plugin.window = hann(plugin.windowSize) / sqrt(plugin.numBufChans);
            
            % rw positions ---- this is the golden ticket
            plugin.anBufWritePos = [1 (cumsum(ones(plugin.numBufChans-1, 1))'*plugin.hopSize)+1];
            plugin.synBufReadPos = plugin.anBufWritePos;
        end
        
        function y = spectral(plugin, x)
            
            % is bufferSize even or odd
            N = 0;
            if mod(length(x), 2) == 0
                N = length(x)/2 + 1;
            else
                N = ceil(length(x)/2);
            end
            
            % enter spectral domain
            X = fft(x);
            X = X(1:N);
            Y = complex(zeros(1, length(X)))';
            
            % sort
            [~, i] = sort(abs(X), 'descend');
%             
%             % truncate to plugin.numPeaks
            Y(i(1:round(plugin.numPeaks))) = X(i(1:round(plugin.numPeaks)));
            
            % return to time domain
            Y = [Y; conj(flip(Y(2:end-1)))];
            y = real(ifft(Y));
            
        end
        
        % asynchronous buffer
        function out = process(plugin,in)

            % input size
            [numSamples, numChannels] = size(in);
            
            % output block
            out = zeros(numSamples, numChannels);
            
            % iterate through samples
            for n = 1:numSamples
                
                % write to buffers
                for b = 1:plugin.numBufChans
                    
                    % write to b channel
                    plugin.anBuf(plugin.anBufWritePos(b), b) = in(n);
                    
                    % if write pos is at end of buffer
                    % perform asynchronous processing
%                     if plugin.anBufWritePos(b) == plugin.bufferSize
                    if plugin.anBufWritePos(b) == 1
                        
                        % copy to synBuf 
                        plugin.synBuf(:, b) = plugin.anBuf(:, b);
                        
                        % apply winfow
                        plugin.synBuf(:, b) = plugin.synBuf(:, b) .* plugin.window;
                        
                        % process
                        plugin.synBuf(:, b) = plugin.spectral(plugin.synBuf(:,b));
                    end
                    
                    out(n, :) = out(n, :) + plugin.synBuf(plugin.synBufReadPos(b), b);
                end
                
                plugin.anBufWritePos = mod(plugin.anBufWritePos, ...
                                        plugin.bufferSize) + 1;
                plugin.synBufReadPos = mod(plugin.synBufReadPos, ...
                                        plugin.bufferSize) + 1;
            end
        end
    end
end
